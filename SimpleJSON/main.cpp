#include <iostream>
#include "JSONParser.h"

using namespace std;

int main()
{
	JSONParser parser;

	parser.CreateFile("TestFile.json");

	parser.CreateVariable("firstName", "John");
	parser.CreateVariable("lastName", "Smith");
	parser.CreateVariable("age", "25");

	parser.CreateObject("address");

	parser.CreateVariable("streetAddress", "21 2nd Street");
	parser.CreateVariable("city", "New York");
	parser.CreateVariable("state", "NY");
	parser.CreateVariable("postalCode", "10021");

	parser.CloseObject();

	parser.CreateArray("phoneNumber");

	parser.CreateObject();
	parser.CreateVariable("type", "home");
	parser.CreateVariable("number", "212 555-1234");
	parser.CloseObject();

	parser.CreateObject();
	parser.CreateVariable("type", "Work");
	parser.CreateVariable("number", "212 646-4567");
	parser.CloseObject();

	parser.CloseArray();

	parser.CloseFile();

	cout << "Parsed Structure read in" << endl;

	parser.OpenFile("TestFile.json");

	JSONObject object = parser.GetParsedObject();

	cout << endl << endl;
	cout << "Reading it into memory:" << endl << endl;

	cout << "First Name: " << object.m_variables["firstName"] << endl;
	cout << "Last Name: " << object.m_variables["lastName"] << endl;
	cout << "Age: " << atoi(object.m_variables["age"].c_str()) << endl;	//convert data to integer

	cout << "Address:" << endl;
	for(auto it : object.m_objects["address"].m_variables)
		cout << "\t" << it.first << ": " << it.second << endl;
	cout << endl;

	for(auto it2 : object.m_arrays["phoneNumber"].m_values)
	{
		for(auto it3 : it2.m_variables)
			cout << it3.first << ": " << it3.second << endl;
		cout << endl;
	}

	cout << endl << endl;
	system("pause");
	return 0;
}